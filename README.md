# eventBeforeTokenEmailExtended#

Allow to update more part in BeforeTokenEmail for LimeSurvey 3.

## Usage

Extend [beforeTokenEmail](https://manual.limesurvey.org/BeforeTokenEmail) with

### Input

- PhpMailer : the complete php mailer (you can use all functoon of PHPMailer)

### Output

- PhpMailer
- CustomHeaders as array
- addCC as array
- addBCC as array

## Contribute and issue

Contribution are welcome, for patch and issue : use [gitlab](https://gitlab.com/SondagesPro/addScriptToQuestion).

Translation can be done at [translate.sondages.pro](https://translate.sondages.pro/projects/addscripttoquestion/). If there are issue with english string : it's a PHP issue, not a language issue.

## Home page & Copyright
- HomePage <http://sondages.pro/>
- Copyright © 2016-2024 Denis Chenu <http://sondages.pro>
- Licence : GNU Affero General Public License <https://www.gnu.org/licenses/agpl-3.0.html>
- [Donate](https://support.sondages.pro/open.php?topicId=12), [Liberapay](https://liberapay.com/SondagesPro/), [OpenCollective](https://opencollective.com/sondagespro) 
